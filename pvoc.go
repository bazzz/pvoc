package pvoc

import (
	"encoding/xml"
	"io"
	"os"
)

// Load reads the Pascal VOC data from the provided io.Reader and returns an Annotation oject. The io.Reader this most likely is a file handle created by calling the os.Open() function.
func Load(input io.Reader) (Annotation, error) {
	annotation := Annotation{}
	data, err := io.ReadAll(input)
	if err != nil {
		return annotation, err
	}
	if err := xml.Unmarshal(data, &annotation); err != nil {
		return annotation, err
	}
	return annotation, nil
}

// Save writes the provided Annotation object as Pascal VOC data to the named file, creating it if necessary.
func Save(filename string, annotation Annotation) error {
	data, err := xml.MarshalIndent(annotation, " ", "  ")
	if err != nil {
		return err
	}
	data = []byte(xml.Header + string(data))
	if err := os.WriteFile(filename, data, os.ModePerm); err != nil {
		return err
	}
	return nil
}
