package pvoc

import "encoding/xml"

type Annotation struct {
	XMLName   xml.Name `xml:"annotation"`
	Folder    string   `xml:"folder"`
	FileName  string   `xml:"filename"`
	Path      string   `xml:"path"`
	Source    Source   `xml:"source"`
	Size      Size     `xml:"size"`
	Segmented int      `xml:"segmented"`
	Objects   []Object `xml:"object"`
}

type Source struct {
	Database string `xml:"database"`
}

type Size struct {
	Width  int `xml:"width"`
	Height int `xml:"height"`
	Depth  int `xml:"depth"`
}

type Object struct {
	Name        string    `xml:"name"`
	Pose        string    `xml:"pose"`
	Truncated   int       `xml:"truncated"`
	Difficult   int       `xml:"difficult"`
	Occluded    int       `xml:"occluded"`
	BoundingBox Rectangle `xml:"bndbox"`
}

type Rectangle struct {
	TopLeftX     int `xml:"xmin"`
	TopLeftY     int `xml:"ymin"`
	BottomRightX int `xml:"xmax"`
	BottomRightY int `xml:"ymax"`
}
